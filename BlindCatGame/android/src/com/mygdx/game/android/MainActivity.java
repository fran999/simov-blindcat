package com.mygdx.game.android;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.media.MediaPlayer;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;

import java.sql.SQLException;

public class MainActivity extends Activity{
    private static MainActivity instance;
    Button playButton;
    Button highscoresButton;
    Button exitButton;
    Intent intentSignInService;
    SignInService signInService;
    private MediaPlayer sound;
    CheckBox cbBlindMode;

    protected ServiceConnection mServercon = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("Service", "Service Connected");
            SignInService.LocalBinder localBinder = (SignInService.LocalBinder)service;
            signInService = localBinder.getServerInstance();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("Service", "Service Disconnected");
        }
    };

    public static MainActivity getInstance(){
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(instance != null){
            instance.finish();
        }

        instance = this;

        /* Play Button */
        playButton = (Button) findViewById(R.id.playButton);

        sound = MediaPlayer.create(MainActivity.this, R.raw.main_menu);
        sound.start();
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sound.stop();
                startGame();
            }
        });

        /* High Scores Button */
        highscoresButton = (Button) findViewById(R.id.highscoresButton);
        highscoresButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ScoresActivity.class));
                sound.stop();
            }
        });

        /* Instructuons Button */
        findViewById(R.id.instructionsButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Instructions.class));
                sound.stop();
            }
        });

        intentSignInService = new Intent(MainActivity.this, SignInService.class);
        startService(intentSignInService);
        //bindService(intentSignInService, mServercon, BIND_AUTO_CREATE);
        //startService(intentSignInService);

        /* Exit Button */
        exitButton = (Button) findViewById(R.id.btnExit);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitGame();
            }
        });
    }

    public void submitScore (String score){
        ScoresDataSource datasource;

        datasource = new ScoresDataSource(this);
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Score scoreObj = datasource.createScore(score);

        datasource.close();
    }

    private void startGame() {
        /* Check Box Blind Mode */
        cbBlindMode = (CheckBox) findViewById(R.id.cbBlindMode);

        Intent i = new Intent(this, AndroidLauncher.class);
        Bundle b = new Bundle();
        b.putBoolean("ischeckboxclicked", cbBlindMode.isChecked()); //Your id
        i.putExtras(b); //Put your id to your next Intent
        startActivity(i);
        instance.finish();
    }

    private void exitGame() {
        sound.stop();
        finish();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}


