package com.mygdx.game.android;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.content.IntentCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;

import java.sql.SQLException;


public class GameOverActivity extends Activity {
    Button playAgainButton;
    Button menuButton;
    TextView scoreTextView;
    private Music yourScore;
    CheckBox cbBlindMode;
    ScoresDataSource scoresDataSource = new ScoresDataSource(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);

        /* Score Text View */
        Bundle b = getIntent().getExtras();
        int scoreValue = b.getInt("scoreresult");
        scoreTextView = (TextView) findViewById(R.id.ScoreTextView);
        scoreTextView.setText("Score: " + String.valueOf(scoreValue));

        sayScore(String.valueOf(scoreValue));

        /* ATUALIZAR O SCORE NA BASE DE DADOS */
        //Score score = scoresDataSource.createScore(String.valueOf(scoreValue));
        submitScore(String.valueOf(scoreValue));

        /* Play Again Button */
        playAgainButton = (Button) findViewById(R.id.PlayAgainButton);
        playAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGame();
            }
        });

        /* Menu Button */
        menuButton = (Button) findViewById(R.id.MenuButton);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainMenu();
            }
        });
    }

    private void startGame() {
        finish();
        cbBlindMode = (CheckBox) findViewById(R.id.cbBlindMode);
        Intent i = new Intent(this, AndroidLauncher.class);
        Bundle b = new Bundle();
        b.putBoolean("ischeckboxclicked", cbBlindMode.isChecked()); //Your id
        i.putExtras(b); //Put your id to your next Intent
        startActivity(i);

    }

    private void goToMainMenu() {
        finish();
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    private void sayScore(String score) {
        yourScore = Gdx.audio.newMusic(Gdx.files.internal("audio/your_score_is.mp3"));
        yourScore.play();

        while(yourScore.isPlaying()) {
            // wait
        }

        // dispose, so the sound doesnt crash in the middle of the game
        yourScore.setOnCompletionListener(new Music.OnCompletionListener() {
            @Override
            public void onCompletion(Music aMusic) {
                yourScore.dispose();
            }
        });

        // say the digits in the score
        for (int i = 0; i < score.length(); i++) {
            final Music digit = Gdx.audio.newMusic(Gdx.files.internal("audio/" + score.charAt(i) + ".mp3"));
            digit.play();

            while(digit.isPlaying()) {
                // wait
            }

            // dispose, so the sound doesnt crash in the middle of the game
            digit.setOnCompletionListener(new Music.OnCompletionListener() {
                @Override
                public void onCompletion(Music aMusic) {
                    digit.dispose();
                }
            });
        }
    }

    public void submitScore (String score){
        ScoresDataSource datasource;

        datasource = new ScoresDataSource(this);
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Score scoreObj = datasource.createScore(score);

        datasource.close();
    }
}
