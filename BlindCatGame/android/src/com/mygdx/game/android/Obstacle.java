package com.mygdx.game.android;

import com.badlogic.gdx.backends.android.AndroidApplication;

/**
 * Created by claudio on 17-12-2015.
 */
public class Obstacle {
    private int id;
    private float posX;
    private float posY;
    private boolean hitThePlayer;

    public Obstacle(int id, float posX, float posY, boolean hitThePlayer) {
        setId(id);
        setPosX(posX);
        setPosY(posY);
        setHitThePlayer(hitThePlayer);
    }

    public int getId() {
        return id;
    }

    public void setId(int objectId) {
        id = objectId;
    }

    public boolean getHitThePlayer() {
        return hitThePlayer;
    }

    public void setHitThePlayer(boolean objectHitThePlayer) {
        hitThePlayer = objectHitThePlayer;
    }

    public float getPosX() {
        return posX;
    }

    public void setPosX(float objectPosX) {
        posX = objectPosX;
    }

    public float getPosY() {
        return posY;
    }

    public void setPosY(float objectPosY) {
        posY = objectPosY;
    }
}