package com.mygdx.game.android;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.plus.Plus;

public class SignInService extends Service implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks{
    private static final String TAG = "SignInTestActivity";

    private static final int OUR_REQUEST_CODE = 49404;

    private GoogleApiClient mPlusClient;

    private boolean mResolveOnFail;

    private ConnectionResult mConnectionResult;

    private ProgressDialog mConnectionProgressDialog;

    private MainActivity instance;

    IBinder mBinder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public SignInService getServerInstance() {
            return SignInService.this;
        }
    }

    public SignInService() {

    }

    @Override
    public void onCreate(){
        super.onCreate();
        instance = MainActivity.getInstance();

        mPlusClient = new GoogleApiClient.Builder(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mResolveOnFail = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        super.onStartCommand(intent, flags, startId);
        Log.i(TAG, "onStartCommand: Service started");
        //Toast.makeText(this, "MyService: onStartCommand: Service started", Toast.LENGTH_LONG).show();
        if(isNetworkAvailable())
        mPlusClient.connect();
        return START_STICKY;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        //Toast.makeText(this, "MyService: onDestroy: Service Destroyed", Toast.LENGTH_LONG).show();
        Log.i(TAG, "Service Destroyed");

        mPlusClient.disconnect();
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.v(TAG, "ConnectionSuccessful");
        Toast.makeText(SignInService.this, "Welcome back " + Plus.PeopleApi.getCurrentPerson(mPlusClient).getName().getGivenName(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v(TAG, "ConnectionSuspended");
        //Toast.makeText(SignInService.this, "ConnectionSuspended", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v(TAG, "ConnectionFailed");
        //Toast.makeText(this, connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();

        if(connectionResult.hasResolution()){
            mConnectionResult = connectionResult;
            startResolution();
        }
    }

    private void startResolution() {
        try {
            mResolveOnFail = false;

            mConnectionResult.startResolutionForResult(instance, OUR_REQUEST_CODE);
            if(mConnectionResult.isSuccess()){
                //Toast.makeText(SignInService.this, "success", Toast.LENGTH_SHORT).show();
            }

            if(mPlusClient.isConnecting()){
                //Toast.makeText(SignInService.this, "connecting", Toast.LENGTH_SHORT).show();
            }
            if(mPlusClient.isConnected()){
                //Toast.makeText(SignInService.this, "connected", Toast.LENGTH_SHORT).show();
            }
            Toast.makeText(SignInService.this, "Failed to Connect to Google+", Toast.LENGTH_SHORT).show();
        } catch (IntentSender.SendIntentException e) {
            if(isNetworkAvailable())
            mPlusClient.connect();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
