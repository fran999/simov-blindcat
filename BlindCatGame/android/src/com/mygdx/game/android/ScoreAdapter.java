package com.mygdx.game.android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PedroFigueiredo(1121 on 12/01/2016.
 */
public class ScoreAdapter extends ArrayAdapter<String>{
    private final Context context;
    private final ArrayList<String> values;

    public ScoreAdapter(Context context, ArrayList<String> values) {
        super(context, R.layout.rowlayout, values);
        this.context = context;
        this.values = values;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.label);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        textView.setText(values.get(position));
        // Change the icon for Windows and iPhone
        if(position == 0){
            imageView.setImageResource(R.drawable.first);
        }else{
            if(position == 1){
                imageView.setImageResource(R.drawable.second);
            }else{
                if(position == 2){
                    imageView.setImageResource(R.drawable.third);
                }else{
                    imageView.setImageResource(R.drawable.transparent);
                }
            }
        }

        return rowView;
    }
}
