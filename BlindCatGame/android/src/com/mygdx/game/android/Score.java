package com.mygdx.game.android;

/**
 * Created by PedroFigueiredo(1121 on 12/01/2016.
 */
public class Score {
    private long id;
    private String score;

    public long getId(){
        return id;
    }

    public void setId(long id){
        this.id = id;
    }

    public String getScore(){
        return score;
    }

    public void setScore (String score){
        this.score = score;
    }

    @Override
    public String toString(){
        return score;
    }
}
