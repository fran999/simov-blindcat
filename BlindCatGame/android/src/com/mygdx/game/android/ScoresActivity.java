package com.mygdx.game.android;

import android.app.ListActivity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ScoresActivity extends ListActivity {
    private ScoresDataSource datasource;
    private MediaPlayer sound;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

        datasource = new ScoresDataSource(this);
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        List<Score> values = datasource.getAllScores();

        //sayScores(values);

        //ArrayList<Integer> valuesInt = new ArrayList<Integer>();
        Integer[] valuesInt = new Integer[values.size()];
        int i = 0;
        for (Score V : values){
            //valuesInt.add(Integer.parseInt(V.getScore()));
            valuesInt[i] = Integer.parseInt(V.getScore());
            i++;
        }
        Arrays.sort(valuesInt, new Comparator<Integer>() {
            @Override
            public int compare(Integer x, Integer y) {
                return y - x;
            }
        });

        //Collections.reverse(valuesInt);

        ArrayList<String> valuesString = new ArrayList<String>();

        for (Integer V : valuesInt){
            valuesString.add(V+"");
        }

        // use the SimpleCursorAdapter to show the
        // elements in a ListView
        //ArrayAdapter<Score> adapter = new ArrayAdapter<Score>(this, R.layout.rowlayout, R.id.label, values);

        ScoreAdapter adapter = new ScoreAdapter(this, valuesString);
        setListAdapter(adapter);

        sayScores(values);
    }

    // Will be called via the onClick attribute
    // of the buttons in main.xml
    public void onClick(View view) {
        @SuppressWarnings("unchecked")
        ArrayAdapter<Score> adapter = (ArrayAdapter<Score>) getListAdapter();
        Score score = null;

        switch (view.getId()) {
            /*
            case R.id.add:
                String[] scores = new String[] { "1", "2", "3" };
                int nextInt = new Random().nextInt(3);
                // save the new score to the database
                score = datasource.createScore(scores[nextInt]);
                adapter.add(score);
                break;

            case R.id.delete:
                if (getListAdapter().getCount() > 0) {
                    score = (Score) getListAdapter().getItem(0);
                    datasource.deleteScore(score);
                    adapter.remove(score);
                }

                break;
                */
        }

        adapter.notifyDataSetChanged();
    }

    private void sayScores(List<Score> v) {
        /* Put list values in an array */

        String[] valuesArray = new String[v.size()];
        for (int i = 0; i < v.size(); i++) {
            valuesArray[i] = v.get(i).getScore();
        }

        int[] valuesArrayInt = new int[valuesArray.length];
        for (int i = 0; i < valuesArray.length; i++) {
            valuesArrayInt[i] = Integer.parseInt(valuesArray[i]);
        }

        Arrays.sort(valuesArrayInt);

        String[] valuesArrayString = new String[valuesArrayInt.length];
        Arrays.sort(valuesArrayInt);
        for (int i = 0; i < valuesArrayInt.length; i++) {
            valuesArrayString[i] = Integer.toString(valuesArrayInt[i]);
        }

        /* Say "high scores" */
        sound = MediaPlayer.create(ScoresActivity.this, R.raw.high_scores);
        sound.start();

        while(sound.isPlaying()) {
            // wait
        }

        /* Say "first place", "second place" or "third place" and the number of points */
        for (int i = 0; i < 3; i++) {
            if(i == 0) {
                sound = MediaPlayer.create(ScoresActivity.this, R.raw.first_place);
                sound.start();

                while(sound.isPlaying()) {
                    // wait
                }

                /* Say the number of points */
                for (int j = 0; j < valuesArrayString[valuesArrayString.length-1].length(); j++) {
                    whatSound(Character.getNumericValue(valuesArrayString[valuesArrayString.length - 1].charAt(j)));
                }

                /* Say "points" */
                sound = MediaPlayer.create(ScoresActivity.this, R.raw.points);
                sound.start();


                while(sound.isPlaying()) {
                    // wait
                }
            } else if(i == 1) {
                sound = MediaPlayer.create(ScoresActivity.this, R.raw.second_place);
                sound.start();

                while(sound.isPlaying()) {
                    // wait
                }

                /* Say the number of points */
                for (int j = 0; j < valuesArrayString[valuesArrayString.length-2].length(); j++) {
                    whatSound(Character.getNumericValue(valuesArrayString[valuesArrayString.length - 2].charAt(j)));
                }

                /* Say "points" */
                sound = MediaPlayer.create(ScoresActivity.this, R.raw.points);
                sound.start();


                while(sound.isPlaying()) {
                    // wait
                }
            } else if(i == 2) {
                sound = MediaPlayer.create(ScoresActivity.this, R.raw.third_place);
                sound.start();

                while(sound.isPlaying()) {
                    // wait
                }

                /* Say the number of points */
                for (int j = 0; j < valuesArrayString[valuesArrayString.length-3].length(); j++) {
                    whatSound(Character.getNumericValue(valuesArrayString[valuesArrayString.length - 3].charAt(j)));
                }

                /* Say "points" */
                sound = MediaPlayer.create(ScoresActivity.this, R.raw.points);
                sound.start();


                while(sound.isPlaying()) {
                    // wait
                }
            }
        }
    }

    private void whatSound(int val) {
        if(val == 0) {
            sound = MediaPlayer.create(ScoresActivity.this, R.raw.zero);
        } else if(val == 1) {
            sound = MediaPlayer.create(ScoresActivity.this, R.raw.one);
        } else if(val == 2) {
            sound = MediaPlayer.create(ScoresActivity.this, R.raw.two);
        } else if(val == 3) {
            sound = MediaPlayer.create(ScoresActivity.this, R.raw.three);
        } else if(val == 4) {
            sound = MediaPlayer.create(ScoresActivity.this, R.raw.four);
        } else if(val == 5) {
            sound = MediaPlayer.create(ScoresActivity.this, R.raw.five);
        } else if(val == 6) {
            sound = MediaPlayer.create(ScoresActivity.this, R.raw.six);
        } else if(val == 7) {
            sound = MediaPlayer.create(ScoresActivity.this, R.raw.seven);
        } else if(val == 8) {
            sound = MediaPlayer.create(ScoresActivity.this, R.raw.eight);
        } else if(val == 9) {
            sound = MediaPlayer.create(ScoresActivity.this, R.raw.nine);
        }

        sound.start();
        while(sound.isPlaying()) {
            // wait
        }
    }

    @Override
    protected void onResume() {
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }

}

