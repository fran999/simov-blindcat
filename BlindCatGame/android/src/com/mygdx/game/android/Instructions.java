package com.mygdx.game.android;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.CheckBox;

public class Instructions extends Activity {
    CheckBox cbBlindMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);

        /* Button Back */
        findViewById(R.id.BtnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
            }
        });

        /* Button Play */
        findViewById(R.id.btnplay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play();
            }
        });
    }

    private void back() {
        finish();
        startActivity(new Intent(Instructions.this, MainActivity.class));
    }

    private void play() {
        finish();
        cbBlindMode = (CheckBox) findViewById(R.id.cbBlindMode);
        Intent i = new Intent(this, AndroidLauncher.class);
        Bundle b = new Bundle();
        b.putBoolean("ischeckboxclicked", cbBlindMode.isChecked()); //Your id
        i.putExtras(b); //Put your id to your next Intent
        startActivity(new Intent(Instructions.this, AndroidLauncher.class));
    }

}
