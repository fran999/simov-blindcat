package com.mygdx.game.android;

import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
//import com.mygdx.game.Render;

public class AndroidLauncher extends AndroidApplication implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
	private static AndroidLauncher instance;
	private static int Score;
	private Context context;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(instance != null){
			instance.finish();
		}
		instance = this;

		Bundle b = getIntent().getExtras();
		boolean blindMode = b.getBoolean("ischeckboxclicked");

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useWakelock = true;
		initialize(new Render(blindMode), config);
	}

	public static AndroidLauncher getInstance(){
		return instance;
	}

	public static void setScore(int value){
		Score = value;
	}

	@Override
	public void onConnected(Bundle bundle) {

	}

	@Override
	public void onConnectionSuspended(int i) {

	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {

	}
}
