package com.mygdx.game.android;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;

import com.badlogic.gdx.audio.Music;

import java.util.Random;

import java.util.ArrayList;


public class Render extends ApplicationAdapter implements SensorEventListener {
    AndroidLauncher instance;
    SensorManager sensorManager;
    Sensor sensorAccelerometer;

    SpriteBatch spriteBatch;
    Texture img;
    Float scrollTimer = (float) 0;
    Sprite sprite;
    Texture charImg;
    Sprite charSprite;

    Float charPosX = 0f;
    Float charPosY = 0f;

    boolean isBlindMode;

    Float x = 0f;
    Float y = 0f;
    Float z = 0f;

    private Music nextlevel;
    private Music collisionSoundEffect;
    private Music gameOverSoundEffect;
    private Music beep;
    private Music left;
    private Music right;

    ArrayList<Obstacle> obstacles = new ArrayList<Obstacle>();
    ArrayList<Sprite> obstacleSprites = new ArrayList<Sprite>();
    //Sprite obstSprite;
    int level = 1;
    Texture obstImg;
    Texture blackScreen;
    Sprite blackScreenSprite;
    Float blackScreenPosX = 0f;
    Float blackScreenPosY = 0f;
    //Obstacle obs = new Obstacle(1, 245, 100, true);
    Chronometer ch = new Chronometer();
    int obstFrequency = 5;
    int flag = 0; // when the compiler enters the next if it would stay for a while dropping multiple obstacles. with this flag it will only drop one obstacle
    int flagCollisionObject = 0; // flag to let the obstacle take 1 point of life and not multiple
    int flagObstacleScore = 0; // flag to let the obstacle take or add 1 point of score and not multiple
    int flagLeftOrRightSound = 0; //flag to let the woman say left or right only once at a time
    int phoneWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
    int phoneHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
    int life = 3; // everytime the player gets it, decrease by 1. when life reaches zero, the player loses
    int currentObjectId = 0;
    int flagLeftOrRight = 0; // this flag is needed so the girl doesn't repeat left or right multiple times when is not needed
    Texture obstImgTmp;
    int score = 0;

    public Render(boolean BlindMode) {
        isBlindMode = BlindMode;
    }

    public void drawRoad() {
        spriteBatch = new SpriteBatch();
        img = new Texture("images/road.png");
        img.setWrap(Texture.TextureWrap.ClampToEdge, Texture.TextureWrap.Repeat);
        sprite = new Sprite(img, -(Gdx.graphics.getWidth() - img.getWidth()) / 2, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        //sprite = new Sprite(img, -(Gdx.graphics.getWidth()/2 - img.getWidth()/2), 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    public void drawCharacter() {
        charImg = new Texture("images/cat.png");
        charSprite = new Sprite(charImg);
        charPosX = (float) (((phoneWidth / 2) - (phoneWidth / 4)) - charSprite.getWidth() / 2);
    }

    public void drawObstacle() {
        obstImg = new Texture("images/dog.png");
        obstacleSprites.add(new Sprite(obstImg));
    }

    public void drawBlackScreen() {
        blackScreen = new Texture("images/blackscreen.png");
        blackScreen.setWrap(Texture.TextureWrap.ClampToEdge, Texture.TextureWrap.Repeat);
        blackScreenSprite = new Sprite(blackScreen, -(Gdx.graphics.getWidth() - img.getWidth()) / 2, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    //@Override
    public void create() {
        obstImgTmp = new Texture("images/dog.png");
        instance = AndroidLauncher.getInstance();
        sensorManager = (SensorManager) instance.getSystemService(Context.SENSOR_SERVICE);
        sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, sensorAccelerometer, SensorManager.SENSOR_DELAY_GAME);
        drawRoad();
        drawCharacter();
        if(isBlindMode)
            drawBlackScreen();
        ch.start();
    }

    //@Override
    public void render() {
        ch.stop();
        //System.out.println("Segundos: " + (int) ch.getSeconds()); // prints elapsed seconds since the beggining of the game
        checkLevel();
        handleObstacles();

        scrollTimer = scrollTimer + Gdx.graphics.getDeltaTime() / 2;
        if (scrollTimer > 1.0f)
            scrollTimer = 0.0f;

        sprite.setV(scrollTimer);
        sprite.setV2(scrollTimer - 2);

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        charSprite.setPosition(charPosX, charPosY);
        for (int i = 0; i < obstacles.size(); i++) {
            obstacleSprites.get(i).setPosition(obstacles.get(i).getPosX(), obstacles.get(i).getPosY());
        }

        spriteBatch.begin();
        sprite.draw(spriteBatch);
        charSprite.draw(spriteBatch);
        for (int i = 0; i < obstacleSprites.size(); i++) {
            obstacleSprites.get(i).draw(spriteBatch);
        }

        if(isBlindMode)
            blackScreenSprite.draw(spriteBatch);

        spriteBatch.end();

        detectCollision();
        moveCharacter();
        moveObstacle();
    }

    private void detectCollision() {
        if (!obstacles.isEmpty()) {
            if (flagCollisionObject != obstacles.get(0).getId() && obstacles.get(0).getPosY() <= charPosY + charImg.getHeight() && ((charPosX > phoneWidth / 2 && obstacles.get(0).getPosX() > phoneWidth / 2) || (charPosX < phoneWidth / 2 && obstacles.get(0).getPosX() < phoneWidth / 2))) {
                Vibrator v = (Vibrator) instance.getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(300);
                life -= 1;
                obstacles.get(0).setHitThePlayer(true);
                collisionSoundEffect = Gdx.audio.newMusic(Gdx.files.internal("audio/collision.mp3"));
                collisionSoundEffect.play();
                // dispose, so the sound doesnt crash in the middle of the game
                collisionSoundEffect.setOnCompletionListener(new Music.OnCompletionListener() {
                    @Override
                    public void onCompletion(Music aMusic) {
                        collisionSoundEffect.dispose();

                    }
                });

                flagCollisionObject = obstacles.get(0).getId();
            }
        }
        if (life <= 0) { // game over
            if(collisionSoundEffect.isPlaying())
                collisionSoundEffect.stop();

            gameOverSoundEffect = Gdx.audio.newMusic(Gdx.files.internal("audio/game_over.mp3"));
            gameOverSoundEffect.play();
            while(gameOverSoundEffect.isPlaying()) {
                // wait
            }
            // dispose, so the sound doesnt crash in the middle of the game
            gameOverSoundEffect.setOnCompletionListener(new Music.OnCompletionListener() {
                @Override
                public void onCompletion(Music aMusic) {
                    gameOverSoundEffect.dispose();

                }
            });

            // call game over activity. if score is less than 0, puts it to 0. send score via parameter to the new activity.
            if(score < 0)
                score = 0;
            Intent intent = new Intent(instance, GameOverActivity.class);
            Bundle b = new Bundle();
            b.putInt("scoreresult", score); //Your id
            intent.putExtras(b); //Put your id to your next Intent
            instance.startActivity(intent);
            instance.finish();
        }
    }

    private void handleObstacles() {
        ch.stop();
        int currentTime = (int) ch.getSeconds();

        if (currentTime % obstFrequency == 0 && currentTime != flag && currentTime != 0) { // ch.getSeconds() is an exact multiple of obstFrequency, so drop an obstacle
            float obsX = randomBetween((float) (((phoneWidth / 2) - (phoneWidth / 4)) - obstImgTmp.getWidth() / 2), (float) (((phoneWidth / 2) + (phoneWidth / 4)) - obstImgTmp.getWidth() / 2));
            float obsY = Resources.getSystem().getDisplayMetrics().heightPixels;
            currentObjectId += 1;
            obstacles.add(new Obstacle(currentObjectId, obsX, obsY, false));
            drawObstacle();
            flag = (int) ch.getSeconds();
        }
    }

    private static float randomBetween(float min, float max) {
        /* Randomly returns one of two integers: min or max */
        int result = new Random().nextInt(1 + (2 - 1) + 1);
        if (result == 1) {
            return min;
        } else {
            return max;
        }
    }

    private void checkLevel() {
        int newLevel = (int) Math.ceil(ch.getSeconds() / 15);

        if (newLevel != level && newLevel != 0) {
            level += 1;
            if (obstFrequency > 1)
                obstFrequency -= 1;
            nextlevel = Gdx.audio.newMusic(Gdx.files.internal("audio/levelup.mp3"));
            nextlevel.play();
            // dispose, so the sound doesnt crash in the middle of the game
            nextlevel.setOnCompletionListener(new Music.OnCompletionListener() {
                @Override
                public void onCompletion(Music aMusic) {
                    nextlevel.dispose();

                }
            });
        }
    }

    private void moveCharacter() {
        if (x > 2) {
            if (charPosX > (float) (phoneWidth / 2)) {
                charPosX = (float) (((phoneWidth / 2) - (phoneWidth / 4)) - charSprite.getWidth() / 2);
                beep = Gdx.audio.newMusic(Gdx.files.internal("audio/beep.mp3"));
                beep.play();
                // dispose, so the sound doesnt crash in the middle of the game
                beep.setOnCompletionListener(new Music.OnCompletionListener() {
                    @Override
                    public void onCompletion(Music aMusic) {
                        beep.dispose();
                    }
                });
            }
        }

        if (x < -2) {
            if (charPosX < (float) (phoneWidth / 2)) {
                charPosX = (float) ((phoneWidth / 2) + (phoneWidth / 4)) - charSprite.getWidth() / 2;
                beep = Gdx.audio.newMusic(Gdx.files.internal("audio/beep.mp3"));
                beep.play();
                // dispose, so the sound doesnt crash in the middle of the game
                beep.setOnCompletionListener(new Music.OnCompletionListener() {
                    @Override
                    public void onCompletion(Music aMusic) {
                        beep.dispose();

                    }
                });
            }
        }
    }

    private void moveObstacle() {
        for (int i = 0; i < obstacles.size(); i++) {
            obstacles.get(i).setPosY(obstacles.get(i).getPosY() - 2);
            if (obstacles.get(i).getPosY() <= 0) {
                if (flagObstacleScore != obstacles.get(0).getId()) {
                    if (obstacles.get(0).getHitThePlayer() == true) {
                        score -= 1;
                    } else {
                        score += level;
                    }
                    flagObstacleScore = obstacles.get(0).getId();
                }
                obstacles.remove(i);
                obstacleSprites.remove(i);
            }
        }

        // make woman say left or right
        if (!obstacles.isEmpty()) {
            if (obstacles.get(0).getPosY() <= charPosY + 130) {
                if (charPosX > (float) (phoneWidth / 2) && obstacles.get(0).getPosX() > (float) (phoneWidth / 2) && flagLeftOrRightSound != obstacles.get(0).getId()) {
                    left = Gdx.audio.newMusic(Gdx.files.internal("audio/left.mp3"));
                    left.play();
                    // dispose, so the sound doesnt crash in the middle of the game
                    left.setOnCompletionListener(new Music.OnCompletionListener() {
                        @Override
                        public void onCompletion(Music aMusic) {
                            left.dispose();

                        }
                    });
                    flagLeftOrRight = obstacles.get(obstacles.size() - 1).getId();
                    flagLeftOrRightSound = obstacles.get(0).getId();
                } else if (charPosX < (float) (phoneWidth / 2) && obstacles.get(0).getPosX() < (float) (phoneWidth / 2) && flagLeftOrRightSound != obstacles.get(0).getId()) {
                    right = Gdx.audio.newMusic(Gdx.files.internal("audio/right.mp3"));
                    right.play();
                    // dispose, so the sound doesnt crash in the middle of the game
                    right.setOnCompletionListener(new Music.OnCompletionListener() {
                        @Override
                        public void onCompletion(Music aMusic) {
                            right.dispose();

                        }
                    });
                    flagLeftOrRight = obstacles.get(obstacles.size() - 1).getId();
                    flagLeftOrRightSound = obstacles.get(0).getId();
                }
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        x = event.values[0];
        y = event.values[1];
        z = event.values[2];
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void resume() {
        sensorManager.registerListener(this, sensorAccelerometer, SensorManager.SENSOR_DELAY_GAME);
        super.resume();
    }

    @Override
    public void pause() {
        sensorManager.unregisterListener(this, sensorAccelerometer);
        super.pause();
    }
}