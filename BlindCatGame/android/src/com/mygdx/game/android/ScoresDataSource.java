package com.mygdx.game.android;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.w3c.dom.Comment;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by PedroFigueiredo(1121 on 12/01/2016.
 */
public class ScoresDataSource {
    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_SCORE };

    public ScoresDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Score createScore(String score) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_SCORE, score);
        long insertId = database.insert(MySQLiteHelper.TABLE_SCORES, null, values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_SCORES,
                allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Score newScore = cursorToScore(cursor);
        cursor.close();
        return newScore;
    }

    public void deleteScore(Score score) {
        long id = score.getId();
        System.out.println("Comment deleted with id: " + id);
        database.delete(MySQLiteHelper.TABLE_SCORES, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public List<Score> getAllScores() {
        List<Score> scores = new ArrayList<Score>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_SCORES,
                allColumns, null, null, null, null, MySQLiteHelper.COLUMN_SCORE + " Desc");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Score score = cursorToScore(cursor);
            scores.add(score);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return scores;
    }

    private Score cursorToScore(Cursor cursor) {
        Score score = new Score();
        score.setId(cursor.getLong(0));
        score.setScore(cursor.getString(1));
        return score;
    }
}
